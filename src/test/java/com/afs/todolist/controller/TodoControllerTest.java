package com.afs.todolist.controller;

import com.afs.todolist.pojo.Todo;
import com.afs.todolist.repository.TodoJPARepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private TodoJPARepository todoJPARepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        todoJPARepository.deleteAll();
    }

    @Test
    void should_return_todos() throws Exception {
        Todo todo = new Todo();
        todo.setText("study react");
        todoJPARepository.save(todo);
        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo.isDone()));

    }

    @Test
    void should_return_todo_when_given_id() throws Exception {
        Todo todo = new Todo();
        todo.setText("study react");
        Todo todoSaved = todoJPARepository.save(todo);
        mockMvc.perform(get("/todo/{id}", todoSaved.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoSaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoSaved.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoSaved.isDone()));
    }

    @Test
    void should_create_todo() throws Exception{
        Todo todo = new Todo();
        todo.setText("study react");
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }

    @Test
    void should_delete_todo_by_id() throws Exception {
        Todo todo = new Todo();
        todo.setText("study react");
        Todo todoSaved = todoJPARepository.save(todo);
        mockMvc.perform(delete("/todo/{id}", todoSaved.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoJPARepository.findById(todoSaved.getId()).isEmpty());
    }

    @Test
    void should_return_todo_when_update_todoState() throws Exception {
        Todo todo = new Todo();
        todo.setText("study react");
        Todo todoSaved = todoJPARepository.save(todo);

        Todo todoUpdate = new Todo();
        todoUpdate.setId(todo.getId());
        todoUpdate.setText(todo.getText());
        todoUpdate.setDone(!todo.isDone());

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(todoUpdate);

        mockMvc.perform(put("/todo/{id}", todoSaved.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoSaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoSaved.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoUpdate.isDone()));

    }


    @Test
    void should_return_todo_when_update_todo_text() throws Exception {
        Todo todo = new Todo();
        todo.setText("study react");
        Todo todoSaved = todoJPARepository.save(todo);

        Todo todoUpdate = new Todo();
        todoUpdate.setId(todo.getId());
        todoUpdate.setText("yyyyy");
        todoUpdate.setDone(todo.isDone());

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(todoUpdate);

        mockMvc.perform(put("/todo/{id}", todoSaved.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoSaved.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("yyyyy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));

    }

}
