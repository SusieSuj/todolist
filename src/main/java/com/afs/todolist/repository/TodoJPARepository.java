package com.afs.todolist.repository;

import com.afs.todolist.pojo.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoJPARepository extends JpaRepository<Todo,Long> {
}
