package com.afs.todolist.controller;

import com.afs.todolist.pojo.Todo;
import com.afs.todolist.service.TodoService;
import com.afs.todolist.service.dto.TodoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/todo")
@RestController
@CrossOrigin
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> findTodos(){
        return todoService.findTodos();
    }

    @GetMapping("/{id}")
    public TodoResponse findTodoById(@PathVariable Long id){
        System.out.println(todoService.findTodoById(id));
        return todoService.findTodoById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody Todo todo) {
        return todoService.createTodo(todo);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id) {
        todoService.delete(id);
    }

    @PutMapping(value = "/{id}")
    public TodoResponse updateTodo(@PathVariable Long id,@RequestBody Todo todo) {
        return todoService.updateTodo(id,todo);
    }

}
