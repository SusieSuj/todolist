package com.afs.todolist.service.dto;

public class TodoRequest {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
