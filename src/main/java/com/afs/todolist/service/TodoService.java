package com.afs.todolist.service;

import com.afs.todolist.exception.TodoNotFoundException;
import com.afs.todolist.pojo.Todo;
import com.afs.todolist.repository.TodoJPARepository;
import com.afs.todolist.service.dto.TodoResponse;
import com.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoJPARepository todoJPARepository;

    public TodoService(TodoJPARepository todoJPARepository) {
        this.todoJPARepository = todoJPARepository;
    }

    public List<TodoResponse> findTodos() {
        return todoJPARepository.findAll().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse findTodoById(Long id) {
        return TodoMapper.toResponse(todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }

    public TodoResponse createTodo(Todo todo) {
        todo.setDone(false);
        return TodoMapper.toResponse(todoJPARepository.save(todo));
    }

    public void delete(Long id) {
        todoJPARepository.deleteById(id);
    }

    public TodoResponse updateTodo(Long id,Todo todo) {
        Todo todoUpdate = todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        todoUpdate.setText(todo.getText());
        todoUpdate.setDone(todo.isDone());
        todoJPARepository.save(todoUpdate);
        return TodoMapper.toResponse(todoUpdate);
    }

    public TodoResponse updateTodoText(Long id, Todo todo) {
        Todo todoUpdate = todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        todoUpdate.setText(todo.getText());
        todoJPARepository.save(todoUpdate);
        return TodoMapper.toResponse(todoUpdate);
    }
}
