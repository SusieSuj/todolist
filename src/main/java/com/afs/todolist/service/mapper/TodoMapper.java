package com.afs.todolist.service.mapper;

import com.afs.todolist.pojo.Todo;
import com.afs.todolist.service.dto.TodoRequest;
import com.afs.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public TodoMapper() {
    }
    public static Todo toEntity(TodoRequest request){
        Todo todo = new Todo();
        BeanUtils.copyProperties(request,todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo too){
        TodoResponse response = new TodoResponse();
        response.setId(too.getId());
        response.setText(too.getText());
        response.setDone(too.isDone());
        return response;
    }
}
