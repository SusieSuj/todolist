create table if not exists todo(
    id BIGINT NOT NULL auto_increment primary key,
    `text` varchar(255) NULL,
    done boolean
);