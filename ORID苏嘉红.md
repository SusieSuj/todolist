# O

- The topic of today's study is: CORS, frontend and backend integration
- Today, when doing the exercise, the front-end and back-end projects that were done before were combined, and there was a cross-domain problem. We learned to use CorsConfiguration to solve cross-domain problems. We can configure which types of requests in the network are allowed by overriding the addMapping method.
- In today's code review, I found a problem. When displaying todoItem, if we enter a long Chinese, the div will automatically increase the height to display the complete, but if we enter English, it will only be displayed as one line. The reason for the error is that the input English has no spaces and defaults to one word, so it does not wrap automatically. The solution is to incorporate the following style on the p tag: word-break: **break-all**.

# R

- Today was fulfilling. The 3 concepts that should be drew  concept maps today have not been touched before. 

# I

- Because  practice step by step before, I feel okay today with practice combining the front and back ends

# D

- It feels like CSS properties are always unrememberable and need to be summarized

